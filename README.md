# ahk: Alternative AutoHotkey keyboard layout for Windows

## By holding CapsLock:
![keyboard layout](Capslock.png?raw=true)

## By holding Ctrl+Shift+Alt:
![keyboard layout](CtrlShiftAlt.png?raw=true)

## By holding Escape:
Use the numbers in the top row as Fn keys (useful for keyboards without Fn keys)

## Installation
First, install AutoHotkey from https://www.autohotkey.com/

Try by double-click, or copy to startup path to install. (You can find the startup path by *Win+R: shell:startup*)

## Modified from:
https://gist.github.com/Danik/5808330#file-capslock_remap_alt-ahk
