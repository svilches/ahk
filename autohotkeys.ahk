﻿; Autohotkey Capslock Remapping Script
; IMPORTANT: Save as UTF8-BOM (For example, with Notepad)
; Modified from:
; Danik
; More info at http://danikgames.com/blog/?p=714
; https://gist.github.com/Danik/5808330#file-capslock_remap_alt-ahk
; danikgames.com
;
; Functionality:
; - Deactivates capslock for normal (accidental) use.
; - Hold Capslock and drag anywhere in a window to move it (not just the title bar).
; - Access the following functions when pressing Capslock:
;     Cursor keys           - J, K, L, I
;     Enter                 - Space
;     Home, PgDn, PgUp, End - H, A, Q, ;
;     Backspace and Del     - U, O
; - Numpad at the right hand resting position when holding Ctrl+Shift+Alt (using keys m,.jkluio and spacebar)
;
; To use capslock as you normally would, you can press WinKey + Capslock


; This script is mostly assembled from modified versions of the following awesome scripts:
;
; # Home Row Computing by Gustavo Duarte: http://duartes.org/gustavo/blog/post/home-row-computing for
; Changes:
; - Does not need register remapping of AppsKey using SharpKeys.
; - Uses normal cursor key layout
; - Added more hotkeys for insert, undo, redo etc.
;
; # Get the Linux Alt+Window Drag Functionality in Windows: http://www.howtogeek.com/howto/windows-vista/get-the-linux-altwindow-drag-functionality-in-windows/
; Changes: The only change was using Capslock instead of Alt. This
; also removes problems in certain applications.




#Persistent
SetCapsLockState, AlwaysOff



; Capslock + jkli (left, down, up, right)

Capslock & j::Send {Blind}{Left DownTemp}
Capslock & j up::Send {Blind}{Left Up}

Capslock & k::Send {Blind}{Down DownTemp}
Capslock & k up::Send {Blind}{Down Up}

Capslock & i::Send {Blind}{Up DownTemp}
Capslock & i up::Send {Blind}{Up Up}

Capslock & l::Send {Blind}{Right DownTemp}
Capslock & l up::Send {Blind}{Right Up}


; Capslock +  (pgdown, pgup, home, end)

Capslock & h::SendInput {Blind}{Home Down}
Capslock & h up::SendInput {Blind}{Home Up}

Capslock & `;::SendInput {Blind}{End Down}
Capslock & `; up::SendInput {Blind}{End Up}

Capslock & q::SendInput {Blind}{PgUp Down}
Capslock & q up::SendInput {Blind}{PgUp Up}

Capslock & a::SendInput {Blind}{PgDn Down}
Capslock & a up::SendInput {Blind}{PgDn Up}

; Capslock + nm (insert, backspace, del)

Capslock & o::SendInput {Blind}{Del Down}
Capslock & u::SendInput {Blind}{BS Down}
Capslock & BS::SendInput {Blind}{BS Down}


; Make Capslock & Enter equivalent to Control+Enter
Capslock & Enter::SendInput {Ctrl down}{Enter}{Ctrl up}


; Make Capslock & Alt Equivalent to Control+Alt
!Capslock::SendInput {Ctrl down}{Alt Down}
!Capslock up::SendInput {Ctrl up}{Alt up}

; Make Capslock+Space -> Enter
Capslock & Space::SendInput {Enter Down}

; Make Capslock+C,V -> Copy, paste
Capslock & c::SendInput {Ctrl Down}{c Down}
Capslock & c up::SendInput {Ctrl Up}{c Up}

Capslock & v::SendInput {Ctrl Down}{v Down}
Capslock & v up::SendInput {Ctrl Up}{v Up}

Capslock & x::SendInput {Ctrl Down}{x Down}
Capslock & x up::SendInput {Ctrl Up}{x Up}

; Make Capslock+n -> ñ
Capslock & n::ñ

; Make Capslock+s -> ß
Capslock & s::ß

; Make Capslock+e -> €
Capslock & e::€

; Make Capslock+d -> °
Capslock & d::Send {ASC 0176}

; Make Capslock+p -> ±
Capslock & p::Send {ASC 0177}

; Numpad using Ctrl+Shift+Alt + m,.jkluio or space

+^!m:: SendInput {Numpad1}
+^!,:: SendInput {Numpad2}
+^!.:: SendInput {Numpad3}
+^!j:: SendInput {Numpad4}
+^!k:: SendInput {Numpad5}
+^!l:: SendInput {Numpad6}
+^!u:: SendInput {Numpad7}
+^!i:: SendInput {Numpad8}
+^!o:: SendInput {Numpad9}


+^!Space:: SendInput {Numpad0}
; +^!Enter:: SendInput {Enter} Removed because of Atom Hydrogen incompatibility
+^!BackSpace:: SendInput {BackSpace}
+^!Tab:: SendInput {Tab}

+^!?::SendInput {Space}
+^!;::Send, {;}
+^!p::Send, {,}
+^!n::Send, {.}


; Make Win Key + Capslock work like Capslock (in case it's ever needed)
#Capslock::
If GetKeyState("CapsLock", "T") = 1
    SetCapsLockState, AlwaysOff
Else
    SetCapsLockState, AlwaysOn
Return

; Use Escape + Number as FNumber
$esc::
KeyWait esc, t0.5
if errorlevel
	{
	Esc & 1:: SendInput {F1}
    Esc & 2:: SendInput {F2}
    Esc & 3:: SendInput {F3}
    Esc & 4:: SendInput {F4}
    Esc & 5:: SendInput {F5}
    Esc & 6:: SendInput {F6}
    Esc & 7:: SendInput {F7}
    Esc & 8:: SendInput {F8}
    Esc & 9:: SendInput {F9}
    Esc & 0:: SendInput {F10}
	}
Else
	send {escape}
KeyWait esc
Return


; Drag windows anywhere
;
; This script modified from the original: http://www.autohotkey.com/docs/scripts/EasyWindowDrag.htm
; by The How-To Geek
; http://www.howtogeek.com

Capslock & LButton::
CoordMode, Mouse  ; Switch to screen/absolute coordinates.
MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin%
if EWD_WinState = 0  ; Only if the window isn't maximized
    SetTimer, EWD_WatchMouse, 10 ; Track the mouse as the user drags it.
return

EWD_WatchMouse:
GetKeyState, EWD_LButtonState, LButton, P
if EWD_LButtonState = U  ; Button has been released, so drag is complete.
{
    SetTimer, EWD_WatchMouse, off
    return
}
GetKeyState, EWD_EscapeState, Escape, P
if EWD_EscapeState = D  ; Escape has been pressed, so drag is cancelled.
{
    SetTimer, EWD_WatchMouse, off
    WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
    return
}
; Otherwise, reposition the window to match the change in mouse coordinates
; caused by the user having dragged the mouse:
CoordMode, Mouse
MouseGetPos, EWD_MouseX, EWD_MouseY
WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
SetWinDelay, -1   ; Makes the below move faster/smoother.
WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
EWD_MouseStartX := EWD_MouseX  ; Update for the next timer-call to this subroutine.
EWD_MouseStartY := EWD_MouseY
return
